'use strict'

export class GarageService {
    constructor(garageCleaningService) {
        this.garageCleaningService = garageCleaningService;
        this.garagesWithCar = new Map();
    }

    registerInGarage(car) {
        const garages = car.isClassic ? GarageService.SECURE_GARAGES : GarageService.SIMPLE_GARAGES;
        const garage = this.findFreeGarageFrom(garages);

        if (!garage) {
            throw new Error('Free garage is not found');
        }

        this.clean(garage);
        this.registerCar(garage, car);

        return garage;
    }

    clean(garage) {
        this.garageCleaningService.clean(garage);
    }

    registerCar(garage, car) {
        this.garagesWithCar.set(garage, car);
    }

    findFreeGarageFrom(garages) {
        return garages.find(garage => this.isGarageFree(garage));
    }

    isGarageFree(garage) {
        return !this.garagesWithCar.has(garage);
    }
}

GarageService.SECURE_GARAGES = [1, 7];
GarageService.SIMPLE_GARAGES = [2, 3, 4, 5, 6];
